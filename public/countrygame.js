function isNumber(o) {
    return !isNaN(o - 0) && o !== null && o.replace(/^\s\s*/, '') !== "" && o !== false;
}
google.load('visualization', '1', { 'packages': ['geochart'] });
google.setOnLoadCallback(init);

var countries = [
    {
        name: "Argentina",
        code: "AR",
        capital: "Buenos Aires"
    },
    {
        name: "Brasil",
        code: "BR",
        capital: "Brasilia"
    },
    {
        name: "Colombia",
        code: "CO",
        capital: "BogotÃ¡"
    },
    {
        name: "French Guiana",
        code: "GF",
        capital: "Cayenne"
    },
    {
        name: "Paraguay",
        code: "PY",
        capital: "AsunciÃ³n"
    },
    {
        name: "Suriname",
        code: "SR",
        capital: "Paramaribo"
    },
    {
        name: "Venezuela",
        code: "VE",
        capital: "Caracas"
    },
    {
        name: "Boliva",
        code: "BO",
        capital: "Sucre"
    },
    {
        name: "Chile",
        code: "CL",
        capital: "Santiago"
    },
    {
        name: "Ecuador",
        code: "EC",
        capital: "Quito"
    },
    {
        name: "Guyana",
        code: "GY",
        capital: "Georgetown"
    },
    {
        name: "Peru",
        code: "PE",
        capital: "Lima"
    },
    {
        name: "Uruguay",
        code: "UY",
        capital: "Montevideo"
    },
    {
        name: "Falkland Islands",
        code: "FK",
        capital: "Stanley"
    },
    {
        name: "South Georgia and the South Sandwich Islands",
        code: "GS",
        capital: "King Edward Point"
    }
];
var country;

var chart;
var options = {
    displayMode: 'regions',
    colorAxis: { minValue: 0, maxValue: 100, colors: ['red', '#d1d2d4'] },
    legend: 'none',
    region: '005', // '142' Asia
    tooltip: {
        trigger: 'none'
    },
    backgroundColor: "#f8f8f8",
    datalessRegionColor: "#d1d2d4"
};

function init() {
    setRandomCountry();
    drawRegionsMap();
}

function setRandomCountry() {
    const randVal = Math.floor(Math.random() * countries.length);
    country = countries[randVal];
    $('#quizpanel').html('<h2>Where is ' + country.name + '?</h2>'
    );
}

function drawRegionsMap() {
    var data = new google.visualization.arrayToDataTable([
        ['Country', 'Popularity'],
        ['Germany', 200],
        ['United States', 300],
        ['Brazil', 400],
        ['Canada', 500],
        ['France', 600]
    ]);
    var chart = new google.visualization.GeoChart(document.getElementById('visualization'));
    var newColor = '#1273b4';//'#c94033';

    google.visualization.events.addListener(chart, 'ready', function () {
        if ($.browser.msie && $.browser.version < 9) {
            $('#max-width:100%;height:auto;').find('iframe').contents().on('hover', 'shape', function (e) {
                if (e.type == 'mouseenter') {
                    if ($(this).prop('fillcolor') != backgroundColor && $(this).prop('fillcolor') != 'none' && typeof ($(this).prop('fillcolor')) != 'undefined') {
                        $(this).attr('baseColor', $(this).prop('fillcolor'));
                        $(this).prop('fillcolor', newColor);
                    }
                }
                else {
                    if (typeof ($(this).attr('baseColor')) != 'undefined') {
                        $(this).prop('fillcolor', "#d1d2d4");
                    }
                }
            });
        }
        else {
            $('#visualization').on('hover', 'path[fill!="#f5f5f5"][fill!="none"]', function (e) {
                if (e.type == 'mouseenter') {
                    $(this).attr('baseColor', $(this).attr('fill'));
                    $(this).attr('fill', newColor);
                }
                else {
                    $(this).attr('fill', "#d1d2d4");
                }
            });
        }
    });
    chart.draw(data, options);
    google.visualization.events.addListener(chart, 'regionClick', function (e) {
        var countrycode = e.region;
        var selected_country = countries.find(function (c) { return c.code == countrycode });
        if(selected_country.code == country.code){
            const toastLiveExample = document.getElementById('toastCorrect');
            const toast = new bootstrap.Toast(toastLiveExample);
            toast.show();
            setRandomCountry();
        }else{
            const toastLiveExample = document.getElementById('toastWrong');
            const toast = new bootstrap.Toast(toastLiveExample);
            toast.show();
        }
    });
    $('#skpBtn').on('click', function(event) {
        setRandomCountry();
      });
};
$(document).ready(function () {
    $("#continents").change(function () {
        options.region = $(this).val();
        drawRegionsMap();
    });
    $(".mark-country").click(function () {
        var country_code = $(this).data("country-code");
        alert("select country: " + country_code);
    });
});